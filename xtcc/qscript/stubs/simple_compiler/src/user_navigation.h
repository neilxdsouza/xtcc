#ifndef user_navigation_h
#define user_navigation_h

enum UserNavigation 
{
	NOT_SET,
	NAVIGATE_PREVIOUS,
	NAVIGATE_NEXT,
	JUMP_TO_QUESTION,
	SAVE_DATA
};

#endif /* user_navigation_h */
