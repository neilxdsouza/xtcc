#include "qscript_debug.h"

namespace qscript_debug
{
	const int32_t DEBUG_UnaryExpression = 0;
	const int32_t DEBUG_Unary2Expression = 0;
	const int32_t DEBUG_BinaryExpression = 0;
	const int32_t DEBUG_Binary2Expression = 0;
	const int32_t DEBUG_DeclarationStatement = 0;
	const int32_t DEBUG_qscript_parser = 0;
	const int32_t DEBUG_RangeQuestion = 0;
	const int32_t DEBUG_NamedStubQuestion = 0;
	const int32_t MAINTAINER_MESSAGES = 1;
	const int32_t DEBUG_LoadData = 0;
	const int32_t DEBUG_IfStatement = 0;
	const int32_t DEBUG_ForStatement = 0;
	const int32_t DEBUG_CompoundStatement = 0;
}
