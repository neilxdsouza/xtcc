There is a new makefile called CustomQscriptMakefile

This was done because I wanted an out of source build and I could not 
configure automake to do things the way I wanted. The automake script
will do an in-source build. However I will lay out how I would like the
final structure to be.

1) make-build-dir is the directory used to create binaries
2) the CustomQscriptMakefile will create binaries in this folder using
the pattern matching rules feature in GNU make 
3) Below is the directory structure of the tree the top-level being
the base of the QScript installation

.
|-- bin
|-- docs
|-- inputs
|-- include
|-- lib
|-- make-build-dir
|   |-- runtime_lib
|-- resources
|-- src

4) src - The source code currently resides in the src directory
5) lib - any libraries which are created will be stored here
6) bin - any binaries created should be stored here
7) there is a target local_install in the CustomQscriptMakefile 
	which will copy the libraries and the binaries to their
	respective folders
	i.e.
	make -f CustomQscriptMakefile local_install

8) We currently create a library libqscript_runtime which
	is used to link the generated questionnaire to the runtime and
	can then be run by the user
9) When running the generated programs you need to specify LD_LIBRARY_PATH=$(QSCRIPT_HOME)/lib
	where $(QSCRIPT_HOME) is the top of the tree of the qscript compiler installation - 
	where the CustomQscriptMakefile resides

10) All the *.h files are copied to the QSCRIPT_HOME/include directory.

11) typical example in trying out one of the inputs in the 
	$(QSCRIPT_HOME)/inputs directory - based on my setup

	Linux:

	1. $ export QSCRIPT_HOME=/media/sda3/home/nxd/xtcc_sourceforge_copy/xtcc/xtcc/qscript/stubs/simple_compiler
	2. $ export PATH=$PATH:$QSCRIPT_HOME/bin
	3. $ cd inputs
	4. 
	   Linux:
	   $ qscript -o -m -n -f inp_jump_test -> inp_jump_test.exe generated
	   Windows
	   $ qscript -o -m -n -s -f inp_jump_test -> inp_jump_test.exe generated
	5. 
	   Linux
	   $ LD_LIBRARY_PATH=$QSCRIPT_HOME/lib ./inp_jump_test.exe -> program runs
	   Windows
	   $ .\inp_jump_test.exe -> program runs
	6. To write out a data file in quantum format:
	   Copy a file from the inputs directory which has a name in this format:
	       *.qtm_data.conf
	   into the same directory where the *.dat files are present

	   example:
	   $cp $QSCRIPT_HOME/inputs/inp_qax_test.qtm_data.conf .

	   rename the "conf" file with the same name that you have given the script

	   Example : 
	   
	   in the inp_qax_test file you will find the following:

     |--------------------- This is the name of the script.
     V
inp_jump_test {
	int32_t i1=1;
	int32_t i2=2;
	int32_t i3;
	i3=10;
	printf("before q1 i1:%d , i2:%d, i3:%d\n", i1, i2, i3);

	q1 "Q1. " sp int32_t (5-8, 1-2, 9-10, 11, 16);
	if(q1 in (1) ){
		i1=10;
	} else {
		i1=20;
	}
	printf("after q1 i1:%d , i2:%d, i3:%d\n", i1, i2, i3);

	q2 "Q2. " sp int32_t (1,2,3,4);
	i1=1001;i2=1002;i3=1003;
	printf("after q2: i1:%d , i2:%d, i3:%d\n", i1, i2, i3);

	q3 "Q3. " sp int32_t (5,6);
	i1=2001;i2=2002;i3=2003;
	printf("after q3: i1:%d , i2:%d, i3:%d\n", i1, i2, i3);
}


	   $ mv inp_qax_test.qtm_data.conf inp_jump_test.qtm_data.conf

           Now run the file as follows

	   $ ./inp_jump_test.exe -q

	   The -q option runs the data file to generate a Quantum data file.

	   The quantum setup will be in a directory setup-inp_jump_test

	7. To generate an SPSS script :
	   Copy a file from the inputs directory which has a name in this format:
	       *.asc_data.conf
	   into the same directory where the *.dat files are present
	   Example:
	   $cp $QSCRIPT_HOME/inputs/inp_combined.asc_data.conf .

	   rename the "conf" file with the same name that you have given the script, see point 6 
	   above to understand where the name of the script is

           Now run the file as follows

	   $ ./inp_jump_test.exe -w

	   The -w option runs the data file to generate a flat ascii data file
	   and an SPSS script.

