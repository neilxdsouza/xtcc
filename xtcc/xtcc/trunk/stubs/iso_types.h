#ifndef xtcc_types_h
#define xtcc_types_h

typedef       char			int8_t;
typedef       short int			int16_t;
typedef       int			int32_t ;
typedef       unsigned char		u_int8_t;
typedef 	unsigned short int	u_int16_t;
typedef  	int			u_int32_t;

#endif /* xtcc_types_h */
