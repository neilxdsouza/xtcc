\entry{Overview of XtCC}{1}{Overview of XtCC}
\entry{Overview of MR}{2}{Overview of MR}
\entry{Scope for Computer assistance in DP}{4}{Scope for Computer assistance in DP}
\entry{Design goals}{5}{Design goals}
\entry{Requirements}{8}{Requirements}
\entry{Installing and building the compiler and associated tools}{9}{Installing and building the compiler and associated tools}
\entry{Testing suite}{10}{Testing suite}
\entry{Overview of XtCC Compiler}{11}{Overview of XtCC Compiler}
\entry{Detailed user manual for each individual program}{12}{Detailed user manual for each individual program}
\entry{Tutorial and Reference documentation of a person who wants to get in and work in developing this software}{13}{Tutorial and Reference documentation of a person who wants to get in and work in developing this software}
