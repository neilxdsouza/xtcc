\numchapentry{Overview of XtCC}{1}{Overview of XtCC}{1}
\numchapentry{Overview of MR}{2}{Overview of MR}{2}
\numchapentry{Scope for Computer assistance in DP}{3}{Scope for Computer assistance in DP}{4}
\numchapentry{Design goals}{4}{Design goals}{5}
\numchapentry{Requirements}{5}{Requirements}{8}
\numchapentry{Installing and building the compiler and associated tools}{6}{Installing and building the compiler and associated tools}{9}
\numchapentry{Testing suite}{7}{Testing suite}{10}
\numchapentry{Overview of XtCC Compiler}{8}{Overview of XtCC Compiler}{11}
\numchapentry{Detailed user manual for each individual program}{9}{Detailed user manual for each individual program}{12}
\numchapentry{Tutorial and Reference documentation of a person who wants to get in and work in developing this software}{10}{Tutorial and Reference documentation of a person who wants to get in and work in developing this software}{13}
